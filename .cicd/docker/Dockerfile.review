FROM node:16-alpine3.14 as builder

RUN apk add python3 python2 make g++ git \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.14/community/ \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.14/main

RUN mkdir /app
WORKDIR /app

COPY package.json .
COPY *lock* ./
RUN yarn install

COPY . .
RUN rm -rf .cicd

ENV BUILDER true
RUN yarn build --skip-plugins @vue/cli-plugin-eslint

FROM nginx:1.19-alpine
COPY --from=builder /app/dist/ /app/
COPY ./.cicd/nginx/nginx.conf  /etc/nginx/nginx.conf

EXPOSE 5000

CMD ["nginx", "-g", "daemon off;"]
