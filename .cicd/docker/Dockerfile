FROM node:16-alpine3.14 as builder

RUN apk add python3 python2 make g++ git \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.14/community/ \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.14/main

RUN mkdir /app
WORKDIR /app

COPY package.json .
COPY *lock* ./
RUN yarn install --production

COPY . .
RUN rm -rf .cicd

ENV BUILDER true
RUN yarn build --skip-plugins @vue/cli-plugin-eslint

# Remove unecessary files
RUN set -eux \
	&& find ./node_modules -type d -iname 'test' -prune -exec rm -rf '{}' \; \
	&& find ./node_modules -type d -iname 'tests' -prune -exec rm -rf '{}' \; \
	&& find ./node_modules -type d -iname 'testing' -prune -exec rm -rf '{}' \; \
	&& find ./node_modules -type d -iname '.bin' -prune -exec rm -rf '{}' \; \
	\
	&& find ./node_modules -type f -iname '.*' -exec rm {} \; \
	&& find ./node_modules -type f -iname 'LICENSE*' -exec rm {} \; \
	&& find ./node_modules -type f -iname 'Makefile*' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.bnf' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.css' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.def' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.flow' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.html' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.info' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.jst' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.lock' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.map' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.markdown' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.md' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.mjs' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.mli' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.png' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.ts' -exec rm {} \; \
	&& find ./node_modules -type f -iname '*.yml' -exec rm {} \;

FROM nginx:1.19-alpine
COPY --from=builder /app/dist/ /app/
COPY ./.cicd/nginx/nginx.conf  /etc/nginx/nginx.conf

EXPOSE 5000

CMD ["nginx", "-g", "daemon off;"]
