{{ range .Versions -}}
<a name="{{ .Tag.Name }}"></a>
## {{ if .Tag.Previous }}[{{ .Tag.Name }}]({{ $.Info.RepositoryURL }}/compare/{{ .Tag.Previous.Name }}...{{ .Tag.Name }}){{ else }}{{ .Tag.Name }}{{ end }}

> {{ datetime "2006-01-02" .Tag.Date }}

{{- range .CommitGroups }}
### {{ .Title }}
{{- range .Commits }}
* {{ if .Scope }}**{{ .Scope }}:** {{ end }}{{ .Subject }}
{{- end }}
{{ end }}
{{ if .Commits -}}
### Jira Issues
{{- range .Commits }}
{{- if .JiraIssue }} 
* [{{ .JiraIssueID }}](https://digitlab.atlassian.net/browse/{{ .JiraIssueID }}) ({{ .Author.Name }}. Email: {{ .Author.Email }}): **{{ .JiraIssue.Summary }}**
  {{- if .JiraIssue.Description }} - {{ .JiraIssue.Description -}} {{- end }} 
{{- end -}}
{{- end }}
{{- end }}

{{ if .RevertCommits -}}
### Reverts

{{ range .RevertCommits -}}
* {{ .Revert.Header }}
{{- end }}
{{- end }}

{{- if .NoteGroups -}}
{{ range .NoteGroups -}}
### {{ .Title }}

{{ range .Notes }}
{{ .Body }}
{{- end }}
{{- end }}
{{- end }}
{{- end -}}