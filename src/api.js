import axios from "axios";

let baseURL = process.env.VUE_APP_BACKEND;

const API = axios.create({
  baseURL: baseURL,
});

API.interceptors.request.use(
  (config) => {
    if (localStorage.getItem("token")) {
      config.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
        "token"
      )}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default API;
