import API from "@/api";

const state = {
  chat: null,
};

const getters = {};

const mutations = {
  SET_CHAT(state, payload) {
    state.chat = payload;
  },
};

const actions = {
  loadEventsChats() {
    return new Promise((resolve, reject) => {
      API.get(`api/chats/events`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadOrganisationsChats() {
    return new Promise((resolve, reject) => {
      API.get(`api/chats/organizations`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  loadChatInfo({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/chats/${data}`)
        .then((response) => {
          commit("SET_CHAT", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  sendChatMessage(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/chats/${data.id}/message`, {
        message: data.message,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
