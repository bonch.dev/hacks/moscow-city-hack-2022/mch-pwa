import API from "@/api";

const state = {};

const getters = {};

const mutations = {};

const actions = {
  loadUserInformation(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/users/${data}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserEvents(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/users/${data}/events`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUsersRating() {
    return new Promise((resolve, reject) => {
      API.get(`api/ratings/users`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
