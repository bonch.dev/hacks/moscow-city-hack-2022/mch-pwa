import { createStore } from "vuex";
import user from "./user.store";
import organisations from "./organisations.store";
import events from "./events.store";
import system from "./system.store";
import search from "./search.store";
import chat from "./chat.store";
import users from "./users.store";

export default createStore({
  modules: {
    system,
    user,
    organisations,
    events,
    search,
    chat,
    users,
  },
});
