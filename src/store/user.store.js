import API from "@/api";

const state = {
  user: "",
};

const getters = {
  getUserCity: function (state) {
    if (state.user.city) {
      return state.user.city.name;
    }

    return "";
  },
};

const mutations = {
  SET_USER_INFO(store, payload) {
    store.user = payload;
  },
};

const actions = {
  createOtpCode(_, data) {
    return new Promise((resolve, reject) => {
      API.post("api/auth/otp", data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loginUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post("api/auth/login", data)
        .then((response) => {
          localStorage.setItem("token", response.data.data.access_token);
          localStorage.setItem(
            "broadcast_token",
            response.data.data.broadcast_token
          );
          commit("SET_USER_INFO", response.data.data.user);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserInformation({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/me")
        .then((response) => {
          commit("SET_USER_INFO", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            localStorage.removeItem("token");
            localStorage.removeItem("broadcast_token");
            window.location.reload();
          }
          reject(error);
        });
    });
  },
  changeUserInformation({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.patch("api/me", data)
        .then((response) => {
          commit("SET_USER_INFO", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  setUserPhoto({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post("api/media", data)
        .then((response) => {
          commit("SET_USER_INFO", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserOrganisations() {
    return new Promise((resolve, reject) => {
      API.get("api/me/organizations")
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserEvents() {
    return new Promise((resolve, reject) => {
      API.get("api/me/events")
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserSubscribes() {
    return new Promise((resolve, reject) => {
      API.get("api/me/subscribes")
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadUserApplications() {
    return new Promise((resolve, reject) => {
      API.get("api/me/applications")
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
