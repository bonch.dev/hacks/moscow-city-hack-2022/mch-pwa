import API from "@/api";

const state = {
  organisations: "",
};

const getters = {};

const mutations = {
  SET_ORGANISATIONS(store, payload) {
    store.organisations = payload;
  },
};

const actions = {
  loadOrganisations({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/organization")
        .then((response) => {
          commit("SET_ORGANISATIONS", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadOrganisationInfo(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/organization/${data}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadOrganisationEvents(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/organization/${data}/events`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  subscribeToOrganisation(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/organization/${data}/subscribe`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  unsubscribeToOrganisation(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/organization/${data}/unsubscribe`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  joinToOrganisation(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/organization/${data}/join`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  leaveFromOrganisation(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/organization/${data}/leave`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadOrganisationMembers(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/organization/${data}/members`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadOrganisationsRating() {
    return new Promise((resolve, reject) => {
      API.get(`api/ratings/organizations`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
