import API from "@/api";

const state = {
  events: "",
};

const getters = {};

const mutations = {
  SET_EVENTS(store, payload) {
    store.events = payload;
  },
};

const actions = {
  loadEvents({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/events")
        .then((response) => {
          commit("SET_EVENTS", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadEventInfo(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/events/${data}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loadEventParticipants(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/events/${data}/participants`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  joinEvent(_, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/events/${data.id}/participate`, { role: data.role || null })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
