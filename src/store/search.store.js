import API from "@/api";

const state = {};

const getters = {};

const mutations = {};

const actions = {
  searchUsers(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/users/search?search=${data}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  searchOrganisations(_, data) {
    return new Promise((resolve, reject) => {
      API.get(`api/organization/search?search=${data}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
