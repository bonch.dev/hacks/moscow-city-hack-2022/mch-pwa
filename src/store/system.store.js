import API from "@/api";

const state = {
  cities: "",
};

const getters = {};

const mutations = {
  SET_CITIES(store, payload) {
    store.cities = payload;
  },
};

const actions = {
  loadCities({ commit }) {
    return new Promise((resolve, reject) => {
      API.get("api/city")
        .then((response) => {
          commit("SET_CITIES", response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
