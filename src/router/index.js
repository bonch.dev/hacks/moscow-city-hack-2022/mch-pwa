import { createRouter, createWebHistory } from "vue-router";
import ClearLayout from "@/layouts/ClearLayout";
import ProfileLayout from "@/layouts/ProfileLayout";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () =>
      import(/* webpackChunkName: "HomeView" */ "../views/HomeView.vue"),
  },
  {
    path: "/auth",
    component: ClearLayout,
    beforeEnter: (to, from, next) => {
      if (localStorage.token) {
        next({ name: "Feed" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Auth",
        component: () =>
          import(
            /* webpackChunkName: "AuthView" */ "../views/Auth/AuthView.vue"
          ),
      },
      {
        path: "login",
        name: "Login",
        component: () =>
          import(
            /* webpackChunkName: "LoginView" */ "../views/Auth/LoginView.vue"
          ),
      },
      {
        path: "onboarding",
        name: "Onboarding",
        component: () =>
          import(
            /* webpackChunkName: "LoginView" */ "../views/Auth/OnboardingView.vue"
          ),
      },
    ],
  },
  {
    path: "/feed",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Feed",
        component: () =>
          import(/* webpackChunkName: "FeedView" */ "../views/FeedView.vue"),
      },
    ],
  },
  {
    path: "/events",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Events",
        component: () =>
          import(/* webpackChunkName: "FeedView" */ "../views/FeedView.vue"),
      },
      {
        path: ":id",
        name: "Event",
        component: () =>
          import(
            /* webpackChunkName: "EventView" */ "../views/Events/EventView.vue"
          ),
      },
    ],
  },
  {
    path: "/organisations",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Events",
        component: () =>
          import(/* webpackChunkName: "FeedView" */ "../views/FeedView.vue"),
      },
      {
        path: ":id",
        name: "Organisation",
        component: () =>
          import(
            /* webpackChunkName: "OrganisationView" */ "../views/Organisations/OrganisationView.vue"
          ),
      },
    ],
  },
  {
    path: "/search",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Search",
        component: () =>
          import(
            /* webpackChunkName: "SearchView" */ "../views/SearchView.vue"
          ),
      },
    ],
  },
  {
    path: "/messages",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Messages",
        component: () =>
          import(
            /* webpackChunkName: "MessagesView" */ "../views/Messages/MessagesView.vue"
          ),
      },
      {
        path: "chats",
        component: ClearLayout,
        children: [
          {
            path: "",
            beforeEnter: (to, from, next) => {
              next({ name: "Messages" });
            },
          },
          {
            path: ":id",
            name: "Chat",
            component: () =>
              import(
                /* webpackChunkName: "ChatView" */ "../views/Messages/ChatView.vue"
              ),
          },
        ],
      },
    ],
  },
  {
    path: "/profile",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Profile",
        component: () =>
          import(
            /* webpackChunkName: "ProfileView" */ "../views/Profile/ProfileView.vue"
          ),
      },
    ],
  },
  {
    path: "/notifications",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        name: "Notifications",
        component: () =>
          import(
            /* webpackChunkName: "NotificationsView" */ "../views/Notifications/NotificationsView.vue"
          ),
      },
    ],
  },
  {
    path: "/users",
    component: ProfileLayout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.token) {
        next({ name: "Auth" });
        return;
      }
      next();
    },
    children: [
      {
        path: "",
        beforeEnter: (to, from, next) => {
          if (!localStorage.token) {
            next({ name: "Profile" });
            return;
          }
          next();
        },
      },
      {
        path: ":id",
        name: "User",
        component: () =>
          import(
            /* webpackChunkName: "NotificationsView" */ "../views/Profile/UserView.vue"
          ),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
