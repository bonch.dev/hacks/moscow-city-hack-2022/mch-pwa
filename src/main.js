import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueQrcode from "@chenfengyuan/vue-qrcode";
import Vue3SimpleHtml2pdf from "vue3-simple-html2pdf";

import "@/scss/app.scss";

createApp(App)
  .use(router)
  .use(store)
  .use(vuetify)
  .use(Vue3SimpleHtml2pdf)
  .component(VueQrcode.name, VueQrcode)
  .mount("#app");
